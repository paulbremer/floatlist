var express = require('express');
var path = require('path');
var serveStatic = require('serve-static');

app = express();
app.use(serveStatic(__dirname));

app.use(require('prerendercloud'));

var port = process.env.PORT || 5000;

app.listen(port);

var uri = 'http://localhost:' + port
console.log('Listening at ' + uri + '\n');
