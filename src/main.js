import Vue from 'vue';
import VueHead from 'vue-head';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import App from './App';
import List from './components/List';
import Map from './components/Map';
import About from './components/About';
import Contact from './components/Contact';

Vue.use(VueHead);
Vue.use(VueRouter);
Vue.use(VueResource);

const routes = [
  { path: '/', component: List },
  { path: '/:country/:list_name', component: List },
  { path: '/map', component: Map },
  { path: '/about-floatlist', component: About },
  { path: '/contact', component: Contact },
  { path: '*', component: List },
];

const router = new VueRouter({
  routes,
  // hashbang: false,
  // history: true,
  // mode: 'history',
});

/* eslint-disable no-new */
new Vue({
  router,
  el: '#app',
  template: '<App/>',
  components: { App },
  data: {
    singleItem: {
      location: {},
    },
    items: null,
    locationImage: '',
  },
});

// Make Vue available globally
window.Vue = Vue;

/* eslint-disable */
(function(){var qs,js,q,s,d=document,gi=d.getElementById,ce=d.createElement,gt=d.getElementsByTagName,id='typef_orm',b='https://s3-eu-west-1.amazonaws.com/share.typeform.com/';if(!gi.call(d,id)){js=ce.call(d,'script');js.id=id;js.src=b+'share.js';q=gt.call(d,'script')[0];q.parentNode.insertBefore(js,q)}})()
/* eslint-enable */
